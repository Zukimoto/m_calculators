<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 19.03.17
 * Time: 16:50
 */
include("header.html");
?>
<div>
    <form enctype="multipart/form-data" class="form-inline" action="" method="post">
        <h3>Расчет даты события, привязанного к дню менструального цикла</h3>
        <form enctype="multipart/form-data" class="form-inline" action="" method="post">
            Дата первого дня последних месячных:
            <br>
            <select class="form-control" id="D" name="D" size="1">
                <option selected="selected" value="01">01</option>
                <option value="01">01</option>
                <option value="02">02</option>
                <option value="03">03</option>
                <option value="04">04</option>
                <option value="05">05</option>
                <option value="06">06</option>
                <option value="07">07</option>
                <option value="08">08</option>
                <option value="09">09</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
                <option value="21">21</option>
                <option value="22">22</option>
                <option value="23">23</option>
                <option value="24">24</option>
                <option value="25">25</option>
                <option value="26">26</option>
                <option value="27">27</option>
                <option value="28">28</option>
                <option value="29">29</option>
                <option value="30">30</option>
                <option value="31">31</option>
            </select>
            <select class="form-control" id="M" name="M" size="1">
                <option selected="selected" value="01">Январь</option>
                <option value="01">Январь</option>
                <option value="02">Февраль</option>
                <option value="03">Март</option>
                <option value="04">Апрель</option>
                <option value="05">Maй</option>
                <option value="06">Июнь</option>
                <option value="07">Июль</option>
                <option value="08">Aвгуст</option>
                <option value="09">Сентябрь</option>
                <option value="10">Oктябрь</option>
                <option value="11">Ноябрь</option>
                <option value="12">Декабрь</option>
            </select>
            <select class="form-control" id="Y" name="Y" size="1">
                <option value="2016">2016
                </option>
                <option selected="selected" value="2017">2017
                </option>
                <option value="2018">2018
                </option>
                <option value="2019">2019
                </option>
                <option value="2020">2020
                </option>
            </select>
            <br>
            Длительность менструального цикла:
            <br>
            <input type="text" name="N" pattern="[0-9]{2}" placeholder="от 18 до 45" value="" min="18" max="45">
            <br>
            День цикла и месяц, когда будет происходить событие (например, запись на операцию)
            <br>
            <select class="form-control" id="day" name="day" size="1">
                <option selected="selected" value="01">01</option>
                <option value="01">01</option>
                <option value="02">02</option>
                <option value="03">03</option>
                <option value="04">04</option>
                <option value="05">05</option>
                <option value="06">06</option>
                <option value="07">07</option>
                <option value="08">08</option>
                <option value="09">09</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
                <option value="21">21</option>
                <option value="22">22</option>
                <option value="23">23</option>
                <option value="24">24</option>
                <option value="25">25</option>
                <option value="26">26</option>
                <option value="27">27</option>
                <option value="28">28</option>
                <option value="29">29</option>
                <option value="30">30</option>
                <option value="31">31</option>
                <option value="32">32</option>
                <option value="33">33</option>
                <option value="34">34</option>
                <option value="35">35</option>
                <option value="36">36</option>
                <option value="37">37</option>
                <option value="38">38</option>
                <option value="39">39</option>
                <option value="40">40</option>
                <option value="41">41</option>
                <option value="42">42</option>
                <option value="43">43</option>
                <option value="44">44</option>
                <option value="45">45</option>
            </select>
            <select class="form-control" id="month" name="month" size="1">
                <option selected="selected" value="01">Январь</option>
                <option value="01">Январь</option>
                <option value="02">Февраль</option>
                <option value="03">Март</option>
                <option value="04">Апрель</option>
                <option value="05">Maй</option>
                <option value="06">Июнь</option>
                <option value="07">Июль</option>
                <option value="08">Aвгуст</option>
                <option value="09">Сентябрь</option>
                <option value="10">Oктябрь</option>
                <option value="11">Ноябрь</option>
                <option value="12">Декабрь</option>
            </select>
            <br>
            <input  class="t_submit" type="submit" name="submit" value="Расчитать (Calculate)">
        </form>
    </form>
</div>
<span>
            <?php if(isset($result)) echo $result ?>
</span>
</div>
</body>
</html>