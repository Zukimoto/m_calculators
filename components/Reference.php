<?php
/**
 * Created by PhpStorm.
 * User: pudchdenis
 * Date: 20.08.16
 * Time: 15:53
 */

namespace components;

use models\Category;
use memento\Category as MementoCategory;

class Reference
{
    protected $_categories = [];

    /**
     * @return Category[]
    */
    public function getCategories()
    {
        if(empty($this->_categories)){
            $categories = MementoCategory::get();
            foreach ($categories as $params){
                $this->_categories[] = new Category($params['id'], $params['code'], $params['name']);
            }
        }
        return $this->_categories;
    }
}