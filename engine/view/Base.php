<?php

namespace engine\view;


class Base
{
    public $basePath;
    
    public function __construct($path)
    {
        $this->basePath = $path;
    }

    public function render($path, $params = ['ker' => 'tlalararwr'])
    {
        extract($params);        
        require_once $this->basePath.'/views/'.$path.'.php';
    }
}