<?php
/**
 * Created by PhpStorm.
 * User: pudchdenis
 * Date: 10.09.16
 * Time: 15:45
 */

namespace memento;


class User
{
    protected static $memento = [
        ['id' => 2, 'username' => 'David', 'email' => 'root@gmail.com'],
        ['id' => 5, 'username' => 'Alex', 'email' => 'meen@gmail.com'],
        ['id' => 6, 'username' => 'Maks', 'email' => 'tetoo@gmail.com'],
        ['id' => 3, 'username' => 'Mark', 'email' => 'loog@gmail.com'],
    ];

    /**
     * @return  array
    */
    public static function get()
    {
        return self::$memento;
    }

    /**
     * @param string $email
     * @return array
    */
    public static function findByEmail($email)
    {
        foreach (self::$memento as $user)
            if($user['email'] == $email)
                return $user;
        return null;
    }

    /**
     * @param string $id
     * @return array
     */
    public static function findById($id)
    {
        foreach (self::$memento as $user)
            if($user['id'] == $id)
                return $user;
        return null;
    }
    
    
    public static function insetSQL($login, $name, $email, $password, $time)
    {
        return "INSERT INTO `users` (`login`, `name`, `email`, `password`, `created_at`) VALUES ('".$login."', '".$name."', '".$email."', '".$password."', '".$time."');";
    }
    

}