<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 19.03.17
 * Time: 13:35
 */

namespace models;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property integer $size_mm
 * @property integer $days
 * @property string $sort_id
 */

class KTR
{
    public $id;

    public $size_mm;

    public $days;

    public $sort_id;

    public function __construct($id, $size_mm, $days, $sort_id)
    {
        $this->id = $id;
        $this->size_mm = $size_mm;
        $this->days = $days;
        $this->sort_id = $sort_id;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return integer
     */
    public function getDaysKTR($id){
        return $this->days;
    }

}