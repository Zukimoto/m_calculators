<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 19.03.17
 * Time: 13:36
 */

namespace models;


class Constants
{
    public $id;

    public $name;

    public $nameRU;

    public $value;

    public $sort_index;

    public function __construct($id, $name, $nameRU, $value, $sort_index)
    {
        $this->id = $id;
        $this->name = $name;
        $this->nameRU = $nameRU;
        $this->value = $value;
        $this->sort_index = $sort_index;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @return string
     */
    public function getValueByName($name){
        return $this->value;
    }

    /**
     * @return string
     */
    public function getNameRuByName($name){
        return $this->nameRU;
    }

}