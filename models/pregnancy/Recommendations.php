<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 19.03.17
 * Time: 13:33
 */

namespace models;

use components\App;

class Recommendations
{
    public $id;

    public $num_week;

    public $title;

    public $text;

    public $sort_id;

    public function __construct($id, $num_week, $title, $text, $sort_id)
    {
        $this->id = $id;
        $this->num_week = $num_week;
        $this->title = $title;
        $this->text = $text;
        $this->sort_id = $sort_id;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return integer
     */
    public function getNum_weekFromId($id){
        return $this->num_week;
    }

    /**
     * надо проверка что если ид 1 то тоже все выводим что бы не было -1 недели
     * это получение предыдущей недели рекомендации и все последующие
     *
     * @return array
     */
    public function getListFromPreview($id){
//        return $this->num_week;
        return null;
    }

    /**
     * получение всех рекомендаций
     *
     * @return array
     */
    public function getListRec(){
//        $app = App::getInstance();
//        $result = $app->db->setSql("SELECT * FROM Recommendations")->all();
//        return $result;
        return null;
    }
}