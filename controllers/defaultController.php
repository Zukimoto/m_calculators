<?php

namespace controllers;

use engine\controllers\Base;
use components\App;
use memento\User;

class defaultController extends Base
{
    public function indexAction()
    {
        $app = App::getInstance();
        $user = User::findById($app->session->get('id'));
        $name = '';
        if(!empty($user))
            $name = $user['username'];

        $this->view->render('default/index', ['name' => $name]);
    }

    public function viewAction()
    {
        echo '<h1>views</h1>';
    }
    
    public function testingAction()
    {
        if(!empty($_GET['lang'])){
            $app = App::getInstance();
            $app->session->set('lg', $_GET['lang']);
        }
        $this->view->render('default/testing');
    }

    public function errorAction()
    {
        echo 'Error';
    }

	public function pregnancyAction()
    {
        $app = App::getInstance();

//        date_default_timezone_set('Europe/Kiev');

        $post_data = $_POST;
        if($post_data)
        $case = $post_data['submit'];

        $day = 86400;
        $week = 604800;
        $this_time = time();
        $days14=1.21e+6;
        $days266 = 266*$day;
        $days280 = 2.419e+7;

        switch ($post_data) {
            case (isset($case) && $case == 'Расчитать (Calculate) 0'):
                break;
            case (isset($case) && $case == 'Расчитать (Calculate) 1'):
                // Дата зачатия
                $date_conception_post = $_POST['D'].'.'.$_POST['M'].'.'.$_POST['Y'];
                $date_conception = strtotime( $date_conception_post);
                $date_conception_views = date('d.m.Y', $date_conception);
                //дата родов
                $birth_date = $date_conception + $days266;
                $date_finish_views = date('d.m.Y', $birth_date);
                // До родов осталось
                $has_left = ($birth_date-$this_time)/$day;
                $has_left_views = round($has_left);
                //Текущая неделя
                $as_it_passed = $this_time - $date_conception;
                $round_days_week = round($as_it_passed/$week);
                //первый день последнего цикла
                $first_day_last_cycle= $this_time - ($round_days_week * $week);
                //предыдущая неделя перед текущей
                $week_rec = $round_days_week-1;
                $recommendations = $app->db->setSql('SELECT title, text  FROM `calculators_recommendations` WHERE `num_week` >='.$week_rec.' ORDER by `num_week`, `sort_id`')->all();

                $this->view->render('default/pregnancy', [
                    'post_data' => $post_data,
                    'first_day_last_cycle' => $first_day_last_cycle,
                    'date_conception_views' => $date_conception_views,
                    'date_finish_views' => $date_finish_views,
                    'has_left_views' => $has_left_views,
                    'round_days_week' => $round_days_week,
                    'recommendations' => $recommendations
                ]);
                break;
            case (isset($case) && $post_data['submit'] == 'Расчитать (Calculate) 2'):
                break;
            case (isset($case) && $case == 'Расчитать (Calculate) 3'):
                // Дата первого дня последнего цикла
                $first_day_last_cycle = $_POST['D'].'.'.$_POST['M'].'.'.$_POST['Y'];
                $first_day_last_cycle_date = strtotime($first_day_last_cycle);
                // Дата зачатия
                $date_conception = $first_day_last_cycle_date + $days14;
                $date_conception_views = date('d.m.Y', $date_conception);
                // Предполагаема дата родов
                $date_finish = date('d-m-Y', strtotime("+280 days"));
                // Дата родов
                $birth_date = $first_day_last_cycle_date + $days280;
                $date_finish_views = nl2br((date('d.m.Y', $birth_date)));
                // До родов осталось
                $has_left = ($birth_date-$this_time)/$day;
                $has_left_views = round($has_left);
                //Текущая неделя
                $as_it_passed = $this_time - $first_day_last_cycle_date;
                $round_days_week = round($as_it_passed/$week) + 1;
                //предыдущая неделя перед текущей
                $week_rec = $round_days_week-1;
                $recommendations = $app->db->setSql('SELECT title, text  FROM `calculators_recommendations` WHERE `num_week` >='.$week_rec.' ORDER by `num_week`, `sort_id`')->all();

                $this->view->render('default/pregnancy', [
                    'post_data' => $post_data,
                    'first_day_last_cycle' => $first_day_last_cycle,
                    'date_conception_views' => $date_conception_views,
                    'date_finish_views' => $date_finish_views,
                    'has_left_views' => $has_left_views,
                    'round_days_week' => $round_days_week,
                    'recommendations' => $recommendations
                ]);
                break;
            case (isset($case) && $post_data['submit'] == 'Расчитать (Calculate) 4'):
                //Текущая неделя
                $round_days_week = $_POST['week'];
                //первый день последнего цикла
                $first_day_last_cycle= $this_time - ($round_days_week * $week);
                // Дата зачатия
                $date_conception = ($this_time - ($round_days_week * $week))+($week*2);
                $date_conception_views = date('d.m.Y', $date_conception);
                // Предполагаема дата родов
                $birth_date = $date_conception + $days266;
                $date_finish_views = date('d.m.Y', $birth_date);
                // До родов осталось
                $has_left = ($birth_date-$this_time)/$day;
                $has_left_views = round($has_left);

                //предыдущая неделя перед текущей
                $week_rec = $round_days_week-1;
                $recommendations = $app->db->setSql('SELECT title, text  FROM `calculators_recommendations` WHERE `num_week` >='.$week_rec.' ORDER by `num_week`, `sort_id`')->all();

                $this->view->render('default/pregnancy', [
                    'post_data' => $post_data,
                    'first_day_last_cycle' => $first_day_last_cycle,
                    'date_conception_views' => $date_conception_views,
                    'date_finish_views' => $date_finish_views,
                    'has_left_views' => $has_left_views,
                    'round_days_week' => $round_days_week,
                    'recommendations' => $recommendations
                ]);
                break;
            case (isset($case) && $post_data['submit'] == 'Расчитать (Calculate) 5'):
                // Дата первого дня шевеления плода
                $first_day_move = $_POST['D'].'.'.$_POST['M'].'.'.$_POST['Y'];
                //Текущая неделя
                $round_days_week = 0;
                if (!empty($_POST['birth'])) {
                    $round_days_week = 22;
                } else{
                    $round_days_week = 20;
                }
                //первый день последнего цикла
                $first_day_last_cycle= $this_time - ($round_days_week * $week);
                // Дата зачатия
                $date_conception = ($this_time - ($round_days_week * $week))+($week*2);
                $date_conception_views = date('d.m.Y', $date_conception);
                // Предполагаема дата родов
                $birth_date = $date_conception + $days266;
                $date_finish_views = date('d.m.Y', $birth_date);
                // До родов осталось
                $has_left = ($birth_date-$this_time)/$day;
                $has_left_views = round($has_left);

                //предыдущая неделя перед текущей
                $week_rec = $round_days_week-1;
                $recommendations = $app->db->setSql('SELECT title, text  FROM `calculators_recommendations` WHERE `num_week` >='.$week_rec.' ORDER by `num_week`, `sort_id`')->all();

                $this->view->render('default/pregnancy', [
                    'post_data' => $post_data,
                    'first_day_last_cycle' => $first_day_last_cycle,
                    'date_conception_views' => $date_conception_views,
                    'date_finish_views' => $date_finish_views,
                    'has_left_views' => $has_left_views,
                    'round_days_week' => $round_days_week,
                    'recommendations' => $recommendations
                ]);
                break;
            case (isset($case) && $post_data['submit'] == 'Расчитать (Calculate) 6'):
                break;
            case (isset($case) && $post_data['submit'] == 'Расчитать (Calculate) 7'):
                break;
            }

        $this->view->render('default/pregnancy');
    }

    public function bmiAction()
    {
        $app = App::getInstance();

        $result_text ="";
        $round_result = 0;

        if (!empty($_POST)) {

            $growth = $_POST['growth'];
            $weight = $_POST['weight'];

            $result = ($growth==0) ? 0 : ($weight*100*100/($growth*$growth));
            $round_result = round ($result);
            $result_text = '</br>Ваш рост: '.$growth.' см';
            $result_text .= '</br>Ваш вес: '.$weight. ' кг';
            $result_text .= '</br>Ваш ИМТ(индекс массы тела) : '.$round_result.'</br>Вывод: ';

            /*До 16 кг/м? Сильный дефицит массы тела
            От 16 до 18.5 кг/м? Дефицит массы тела
            От 18.5 до 25 кг/м? Нормальная масса тела
            От 25 до 30 кг/м? Состояние, предшествующее ожирению (избыточный вес)
            От 30 до 35 кг/м? Первая стадия ожирения
            От 35 до 40 кг/м? Вторая стадия ожирения
            От 40 кг/м? и выше Третья стадия ожирения*/

            if($round_result<16){
                $result_text.= "Сильный дефицит массы тела ";

            }if($round_result<18.5 && $round_result>=16){
                $result_text.= "Дефицит массы тела ";

            }if($round_result<25 && $round_result>=18.5){
                $result_text.= "Нормальная масса тела ";

            }if($round_result<30 && $round_result>=25){
                $result_text.= "Состояние, предшествующее ожирению (избыточный вес) ";

            }if($round_result<35 && $round_result>=30){
                $result_text.= "Первая стадия ожирения ";

            }if($round_result<40 && $round_result>=35){
                $result_text.= "Вторая стадия ожирения ";

            }if($round_result>=40){
                $result_text.= "Третья стадия ожирения ";

            }
        }

        $this->view->render('default/bmi', ['result' => $result_text]);
    }

    public function periodAction()
    {
        $app = App::getInstance();
        $result_text ="";

    /*Калькулятор «Расчет даты события, привязанного к дню менструального цикла»
    Поля ввода: 
    1. Первый день последней менструации
    2. Длительность менструального цикла (от 18 до 45 дней)
    3. Текущая дата
    4. День цикла и месяц, когда будет происходить событие (например, запись на операцию)
    Выдача: дата*/

        if (!empty($_POST)) {
            // Дата первого дня последнего цикла
            $first_day_last_cycle = $_POST['D'].'.'.$_POST['M'].'.'.$_POST['Y'];
            $first_day_last_cycle_date = strtotime($first_day_last_cycle);
            // Средняя длинна цикла
            $length_period = $_POST['N']=0 ? $_POST['N'] : 28;
            // Теперешняя дата
            $this_time = time();
            //Дата начала месяца и конец месяца когда будет происходить событие (например, запись на операцию):
            $day_period = $_POST['day'];
             //в секундах
            $day = 86400;//один день в секундах
            $day_period_sec = $day_period * $day;
            
            $finish_month_event = $_POST['month'];

            $date_start_month = "01".'.'.$_POST['month'].'.'.$_POST['Y'];
            $date_start_month_date = strtotime($date_start_month);
            
            $month = $_POST['month'];
            $date_finish_month = "30".'.'.$month.'.'.$_POST['Y'];
            $date_finish_month_date = strtotime($date_finish_month);
            /*
             * Расчеты
             * */
            $result_text = '</br>Дата первого дня последних месячных: '.$first_day_last_cycle;
            $result_text.= '</br>Длительность менструального цикла: '.$length_period;
            $result_text.= '</br>День цикла, когда будет происходить событие: '.$day_period;
            $result_text.= '</br>Месяц, когда будет происходить событие: '.$month;
            $result_text.= '</br>Начало месяца события: '.$date_start_month;
            $result_text.= '</br>Конец месяца события: '.$date_finish_month;
            $result_text.= '</br></br>';
            // Дата первого дня последнего цикла + День цикла
            $date0 = $first_day_last_cycle_date + $day_period_sec;
            $nom = 1;
            while($date0 < $date_finish_month_date && $nom<1000){
                $diff = ($date_finish_month_date -$date0)/$day;
                //$result_text.= '</br>nom:'.$nom;
                //$result_text.= '</br>$date0:'.date('d.m.Y', $date0);
                if($date0 > $date_start_month_date) 
                    $result_text.= 'Расчетная дата события: '.date('d.m.Y', $date0);
                $date0 = $date0 + $day*$length_period;
                $nom++;
            }
        }
        $this->view->render('default/period', ['result' => $result_text]);
    }
}