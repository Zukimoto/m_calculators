<?php
/**
 * Created by PhpStorm.
 * User: pudchdenis
 * Date: 10.09.16
 * Time: 14:14
 */

namespace controllers;

use engine\controllers\Base;
use components\App;
use memento\User;

class userController extends Base
{
    public function loginAction()
    {
        $app = App::getInstance();
        $app->session->set('id', null);
        if($app->request->isPost){
            if(isset($_POST['email'])){
                $result = User::findByEmail($_POST['email']);
                if(!empty($result)){
                    $app->session->set('id', $result['id']);
                    $app->request->redirectTo('/');
                }
            }
        }

        $this->view->render('user/login');
    }
    
    public function registrationAction()
    {
        $app = App::getInstance();

        if($app->request->isPost) {
            $result = $app->db->setSql(User::insetSQL($_POST['name'], $_POST['login'], $_POST['email'], $_POST['password'], time()))->exec();
            $app->request->redirectTo('/user/userlist');
        }
        
        $this->view->render('user/register');
    }

    public function userlistAction()
    {
        $app = App::getInstance();

        $users = $app->db->setSql('SELECT * FROM users')->all();

        $this->view->render('user/list', [
            'users' => $users
        ]);
    }
}