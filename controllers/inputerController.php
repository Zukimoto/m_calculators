<?php
namespace controllers;

use engine\controllers\Base;
use components\App;

class inputerController extends Base 
{
    
    public function indexAction()
    {
        $app = App::getInstance();
        if($app->request->isPost) {
            echo 'Post';
        }

        $this->view->render('inputer/index');
    }

    public function creatorAction()
    {
        $app = App::getInstance();
        if($app->request->isPost) {
            echo 'Post';
        }
    }
}